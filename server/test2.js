const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

var createHandler = require('node-gitlab-webhook');
var handler = createHandler([ // multiple handlers
    { path: '/webhook1', secret: 'secret1' },
    { path: '/webhook2', secret: 'secret2' }
])

const app = express();

const sigHeaderName = 'X-Hub-Signature-256';
const sigHashAlg = 'sha256';
const secret = 'ABCD1234';

// middleware

app.use(bodyParser.json());
app.use(cors());
app.all('*', function (req, res, next) {
    handler(req, res, function (err) {        
        console.log(req.url);
        console.log('dsdfsdfsdfsdf');
    });
    next(); // pass control to the next handler
});

const posts = require('./routes/api/posts');
app.use('/api/posts', posts);
app.get('/', function(req, res) {
    res.send('webserver api');
});
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server started on port ${port}`));


handler.on('error', function (err) {
  console.error('Error:', err.message)
})

handler.on('push', function (event) {
  console.log(
    'Received a push event for %s to %s',
    event.payload.repository.name,
    event.payload.ref
  )
  switch (event.path) {
    case '/webhook1':
        console.log(JSON.stringify(event));
      // do sth about webhook1
      break
    case '/webhook2':
      // do sth about webhook2
      break
    default:
      // do sth else or nothing
      break
  }
});