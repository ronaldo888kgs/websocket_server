const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');
const WebSocket = require('ws');
const app = express();

const wss = new WebSocket.Server({port: 8000});


wss.on('connection', ws => {
    ws.on('message', message => {
      console.log(`Received message => ${message}`)
    })
    ws.send('Hello! Message From Server!!')
  })

const sigHeaderName = 'X-Hub-Signature-256';
const sigHashAlg = 'sha256';
const secret = 'ABCD1234';

// middleware

app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/apis');
app.use('/api', posts);


app.post("/log-github-webhook", async function(req, res) {
    var objBody = req.body;
    console.log(objBody);
    console.log('from web hook', JSON.stringify(objBody));
    wss.clients.forEach(client => {
        if(client.readyState == WebSocket.OPEN)
            client.send(objBody);
    })
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`server started on port ${port}`));