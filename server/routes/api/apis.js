const express = require('express');
const https = require('https');
const router = express.Router();
const axios = require('axios');
axios.defaults.timeout = 30000;
axios.defaults.httpsAgent = new https.Agent({ keepAlive: true });
// get posts
router.get('/', (req, res) => {
  res.send('web apis');
});

router.post('/get-repositories', async function(req, res){
  let url = `https://gitlab.com/api/v4/projects/${req.body.projectID}/issues`;
  axios.get(url).then(response => {
    if(response.status == 200)
    {
      res.status(200).send({
        data:response.data
      });  
    }
    else{
      res.status(response.status).send({
        data:JSON.stringify(response)
      });  
    }
    
  }).catch(error=>{
    res.status(500).send({
      message:"invalid operation"
    });  
  })
});
  
module.exports = router;